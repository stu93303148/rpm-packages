## Install Steps

- cd /usr/pgsql-9.6/bin
- ./postgresql96-setup initdb
- systemctl enable postgresql-9.6.service
- systemctl start postgresql-9.6.service

[RPM Download Link](https://download.postgresql.org/pub/repos/yum/9.6/redhat/)


